//
//  LoginUITest.swift
//  BDD_Example_With_SwiftUITests
//
//  Created by Jorge luis Menco Jaraba on 6/18/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import XCTest

class LoginUITest: XCTestCase, LoginUITestType {
    
    override func setUp() {
        super.setUp()
    }
    
    struct constans {
        static let userName = "Pedro"
        static let password = "123"
        static let incorrectPassword = "1324dv"
    }
    
    func testWhenAppIsLauchedUserShouldSeeLoginScreem() {
        giveTheAppIsLaunched()
        thenUserSeeLoginScreem()
    }
    
    func testWhenUserEnteredEmailAndPasswordGetWelcomeMessage() {
        giveTheAppIsLaunched()
        thenUserSeeLoginScreem()
        whenUserEnteredUserAndPassword(userName: constans.userName,
                                       password: constans.password)
        whenUserDoesTapInLoginButton()
        thenUserGetWelcomeMessage()
    }
    
    func testWhenUserEnteredIncorrectUserAndPasswordGetFailureMessage() {
        giveTheAppIsLaunched()
        thenUserSeeLoginScreem()
        whenUserEnteredUserAndPassword(userName: constans.userName, password: constans.incorrectPassword)
        whenUserDoesTapInLoginButton()
        thenUserGetFailureMessage()
    }
}
