//
//  Login+Elements.swift
//  BDD_Example_With_SwiftUITests
//
//  Created by Jorge luis Menco Jaraba on 6/19/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import XCTest


enum LoginElements {
    
    static let loginTitleLabel = XCUIApplication().staticTexts.matching(identifier: LoginAccesibilityIDS.loginTitle).firstMatch
    static let usernameTexField = XCUIApplication().textFields.matching(identifier: LoginAccesibilityIDS.username).firstMatch
    static let passwordTextField = XCUIApplication().textFields.matching(identifier: LoginAccesibilityIDS.password).firstMatch
    
    static let loginButton = XCUIApplication().buttons.matching(identifier: LoginAccesibilityIDS.loginButton).firstMatch
}
