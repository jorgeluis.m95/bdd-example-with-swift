//
//  Login+Protocol.swift
//  BDD_Example_With_SwiftUITests
//
//  Created by Jorge luis Menco Jaraba on 6/18/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import XCTest

protocol LoginUITestType {
    func testWhenAppIsLauchedUserShouldSeeLoginScreem()
    func testWhenUserEnteredEmailAndPasswordGetWelcomeMessage()
    func testWhenUserEnteredIncorrectUserAndPasswordGetFailureMessage()
}

extension LoginUITestType {
    
    func giveTheAppIsLaunched() {
        XCUIApplication().launch()
        startUIKitLoginComponents()
    }
    
    func thenUserSeeLoginScreem() {
        XCTAssert(LoginElements.loginTitleLabel.exists)
        XCTAssert(LoginElements.usernameTexField.exists)
        XCTAssert(LoginElements.passwordTextField.exists)
        XCTAssert(LoginElements.loginTitleLabel.exists)
    }
    
    func whenUserEnteredUserAndPassword(userName: String, password: String) {
        LoginElements.usernameTexField.tap()
        typeTextInElement(element: LoginElements.usernameTexField, text: userName)
        LoginElements.passwordTextField.tap()
        typeTextInElement(element: LoginElements.passwordTextField, text: password)
    }
    func whenUserDoesTapInLoginButton() {
        LoginElements.loginButton.tap()
    }
    
    func thenUserGetWelcomeMessage() {
       XCTAssert(XCUIApplication().alerts.firstMatch.exists)
    }
    
    func thenUserGetFailureMessage() {
       XCTAssert(XCUIApplication().alerts.firstMatch.exists)
    }
    
    private func typeTextInElement(element: XCUIElement, text: String) {
        element.typeText(text)
    }
    
    private func startUIKitLoginComponents() {
        XCUIApplication().staticTexts.matching(identifier: LoginAccesibilityIDS.loginTitle)
        XCUIApplication().textFields.matching(identifier: LoginAccesibilityIDS.username)
        XCUIApplication().textFields.matching(identifier: LoginAccesibilityIDS.password)
        XCUIApplication().buttons.matching(identifier: LoginAccesibilityIDS.loginButton)
    }
}
