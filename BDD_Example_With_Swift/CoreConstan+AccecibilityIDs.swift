//
//  CoreConstan+AccecibilityIDs.swift
//  BDD_Example_With_Swift
//
//  Created by Jorge luis Menco Jaraba on 6/19/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

public struct LoginAccesibilityIDS {
    static let loginTitle = "Login"
    static let username = "username"
    static let password = "password"
    static let loginButton = "loginButton"
}
