//
//  LoginViewController.swift
//  BDD_Example_With_Swift
//
//  Created by Jorge luis Menco Jaraba on 6/18/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginTitleLable: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    struct InnerConstans {
        static let correctUsername = "pedro"
        static let correctPassword = "123"
        static let succesTitle = "Success"
        static let errorTitle = "Error"
        static let successLoginMessage = "Login success"
        static let errorLoginMessage = "Login Error"
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setAccesibilityIDs()
    }
    
    private func setAccesibilityIDs() {
        loginTitleLable.accessibilityIdentifier = LoginAccesibilityIDS.loginTitle
        userNameTextField.accessibilityIdentifier = LoginAccesibilityIDS.username
        passwordTextField.accessibilityIdentifier = LoginAccesibilityIDS.password
        loginButton.accessibilityIdentifier = LoginAccesibilityIDS.loginButton
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
        guard let userName = userNameTextField.text,
            let password = passwordTextField.text else {
                return
        }
        
        validateUserAndPassword(userName: userName, password: password)
    }
    
    private func creteAlertMessage(title: String, message: String) -> UIAlertController {
        let alertControler = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        alertControler.addAction(okAction)
        alertControler.addAction(cancelAction)
        
        return alertControler
    }
    
    private func validateUserAndPassword(userName: String, password: String) {
        var alertLogin = UIAlertController()
        if userName == InnerConstans.correctUsername && password == InnerConstans.correctPassword {
            alertLogin = creteAlertMessage(title: InnerConstans.succesTitle, message: InnerConstans.successLoginMessage)
        } else {
            alertLogin = creteAlertMessage(title: InnerConstans.errorTitle, message: InnerConstans.errorLoginMessage)
        }
        
        present(alertLogin,animated: true)
    }
}
